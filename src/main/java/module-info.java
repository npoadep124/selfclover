open module ru.npoa.dep124.selfclover {

    requires hibernate.c3p0;
    requires hibernate.core;
    requires hibernate.envers;
    requires hibernate.jpa;
    requires joml;
    requires jsch;
    requires material.ui.swing;
    requires org.lwjgl;
    requires org.lwjgl.egl;
    requires org.lwjgl.glfw;
    requires org.lwjgl.jawt;
    requires org.lwjgl.opengles;
    requires org.lwjgl.natives;
    requires org.lwjgl.opengles.natives;
    requires sqlite.jdbc;

    requires org.freedesktop.dbus;
    requires com.bric.colorpicker;
    requires jxmapviewer2;
    requires plugin_core;
    requires java.desktop;
    requires java.naming;
    requires java.sql;
    requires jdk.accessibility;
    requires java.xml.bind;

    requires com.sun.jna.platform;
    requires com.sun.jna;
    requires jnaerator;
    requires LWJmodule;

}
