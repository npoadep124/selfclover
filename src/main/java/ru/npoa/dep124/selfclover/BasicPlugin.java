package ru.npoa.dep124.selfclover;

import org.joml.Vector2i;
import org.liquidengine.legui.component.Component;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.system.renderer.AbstractRendererProvider;
import org.liquidengine.legui.system.renderer.nvg.NvgRendererProvider;
import parsing.LayoutTheme;
import ru.npoa.dep124.selfclover.view.*;
import ru.npoa.dep124.plugin_core.BaseViewControllerInterface;
import ru.npoa.dep124.plugin_core.KustControlInterface;
import ru.npoa.dep124.plugin_core.MainFrameViewInterface;
import ru.npoa.dep124.plugin_core.config.Net;
import ru.npoa.dep124.plugin_core.config.ScreenSize;
import ru.npoa.dep124.plugin_core.dialogs.DialogInputTextInterface;
import ru.npoa.dep124.plugin_core.map.ModeAB;

import java.io.InputStream;


public class BasicPlugin implements MainFrameViewInterface {

    private Vector2i sizeFrame = new Vector2i(1024, 770);

//    private MainView mainView;
//    private MainViewController mainViewController;
//
//    private CoursePointerView coursePointerView;
//    private Panel1View panel1View;
//    private CoursePointerController coursePointerController;

    private NewMainView newMainView;
    private NewMainViewController newMainViewController;

    public static final String NAME_COURSE_POINTER = "course_pointer";

    public BasicPlugin() {
//        mainView = new MainView();
//        mainViewController = new MainViewController();
//        mainViewController.setView(mainView);
//        mainView.setPosition(0, 0);
//
//        coursePointerView = new CoursePointerView();
//        panel1View = new Panel1View();
//        coursePointerController = new CoursePointerController();
//        coursePointerController.setView(panel1View);

//        mainView.addPanel(NAME_COURSE_POINTER, panel1View);
        newMainView = new NewMainView();
        newMainView.initComp();
        newMainViewController = new NewMainViewController();
        resizePanels();
    }

//    @Override
    public ComponentXML getMainView() {
        return newMainView;
    }

    @Override
    public void setSizeMainView(float width, float height) {
        sizeFrame = new Vector2i((int) width, (int) height);
        resizePanels();
    }

    private void resizePanels() {
        newMainView.setSize(1024, 770);
//        coursePointerView.resizeAll();
//        newMainView.resizeAll();
    }

    @Override
    public BaseViewControllerInterface getBaseViewControllerInterface() {
        return null;
    }

    public LayoutTheme initializationLayoutTheme() {
        return new LayoutTheme(BasicPlugin.class.getResourceAsStream("/values/colors.xml"),
                BasicPlugin.class.getResourceAsStream("/values/dimens.xml"),
                (path) -> BasicPlugin.class.getResourceAsStream(path));
    }

    @Override
    public String getTitleWindows() {
        return "Базовая реализация";
    }

    @Override
    public <C extends AbstractRendererProvider> C getRendererProvider() {
        return (C) new NvgRendererProvider();
    }

    @Override
    public Vector2i getSizeFrame() {
        return sizeFrame;
    }

    @Override
    public Net getNet() {
        return null;
    }

    @Override
    public InputStream getIni() {
        return null;
    }

    @Override
    public String getIniFileName() {
        return "temp.ini";
    }

    @Override
    public void setKustInterface(KustControlInterface kustInterface) {

    }


    @Override
    public void updateKustDevice() {

    }

    @Override
    public DialogInputTextInterface getDialogInputText() {
        return null;
    }

    @Override
    public void setMFBButtonMode(ModeAB mode) {

    }
}
