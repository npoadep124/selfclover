package ru.npoa.dep124.selfclover.view;

import annotations.LayoutComponentBind;
import annotations.LayoutIncludeBind;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Label;

public class SprayerInfoView extends ComponentXML {

    public SprayerInfoView() {
        super((path) -> SprayerInfoView.class.getResourceAsStream(path));
    }

}
