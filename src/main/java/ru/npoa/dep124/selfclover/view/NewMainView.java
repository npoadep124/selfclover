package ru.npoa.dep124.selfclover.view;

import annotations.LayoutComponentBind;
import annotations.LayoutIncludeBind;
import annotations.LayoutPathBind;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import org.liquidengine.legui.system.renderer.nvg.util.NvgColorUtil;
import parsing.LayoutTheme;
import org.joml.Vector4f;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static org.liquidengine.legui.event.MouseClickEvent.MouseClickAction.PRESS;


//@LayoutPathBind("/layout/layout_main_view.xml")
@LayoutPathBind("/layout/layout_main_view.xml")
public class NewMainView extends ComponentXML {

    @LayoutIncludeBind(xmlId = "windowcell", xmlClass = SprayerInfoView.class)
    private SprayerInfoView windowcell;

    @LayoutIncludeBind(xmlId = "includefirst", xmlClass = IncludeFirstView.class)
    private IncludeFirstView includefirst;

    @LayoutIncludeBind(xmlId = "time", xmlClass = TimeView.class)
    private TimeView time;

    @LayoutIncludeBind(xmlId = "settings", xmlClass = MonitorSettingsView.class)
    private MonitorSettingsView settings;

    @LayoutComponentBind()
    private Label labelTime;

    @LayoutComponentBind()
    private Label labelDate;

    @LayoutComponentBind()
    private Panel labelSprayer;

    @LayoutComponentBind()
    private Panel labelSprayerInfo;

    @LayoutComponentBind()
    private Panel labelTimeView;

    @LayoutComponentBind()
    private Panel labelSettings;

    @LayoutComponentBind()
    private Label labelValue1;

    @LayoutComponentBind()
    private Label labelValue2;

    @LayoutComponentBind()
    private Label labelValue3;

    @LayoutComponentBind()
    private Panel panelTop;

    private Timer timer = new Timer();
    private TimerTask taskTimer;
    private boolean taskRun = false;

    private Vector4f clickColor = new Vector4f(0.22f, 0.66f, 0.33f, 1f);

public NewMainView() {
    super((path) -> NewMainView.class.getResourceAsStream(path));

//        Panel sjs = new Panel();
//        windowcell.setVisible(false);
//        includefirst.setVisible(false);
//        settings.setVisible(false);
//        time.setVisible(false);

}
public void initComp(){
        DateFormat formatOnlyTime = new SimpleDateFormat("HH:mm");
        DateFormat formatDate = new SimpleDateFormat("dd LLL E");

//        Label jj = new Label();
//        jj.getTextState().setText().setText();

            taskTimer = new TimerTask() {
                @Override
                public void run() {
//                    System.out.println("");
                    labelTime.getTextState().setText(formatOnlyTime.format(new Date()));
                    labelDate.getTextState().setText(formatDate.format(new Date()));
//                    labelDateandTime.getTextState().setText(formatDateandTime.format(new Date()));
                }
            };
            timer = new Timer();
            taskRun = true;
            timer.scheduleAtFixedRate(taskTimer, 0, 1000); // запуск таймера


        labelSprayer.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                windowcell.setVisible(false);
                includefirst.setVisible(true);
                time.setVisible(false);
                settings.setVisible(false);
                panelTop.setRolled(true);

                labelSprayer.getStyle().getBackground().setColor(clickColor);
                labelSprayerInfo.getStyle().setBackground(null);
                labelTimeView.getStyle().setBackground(null);
                labelSettings.getStyle().setBackground(null);
            }
        });

        labelSprayerInfo.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                windowcell.setVisible(true);
                includefirst.setVisible(false);
                time.setVisible(false);
                settings.setVisible(false);
                panelTop.setRolled(false);
//                sjs.getStyle().getBackground().setColor();
//                sjs.getStyle().setBackground(null);
                labelSprayer.getStyle().setBackground(null);
                labelSprayerInfo.getStyle().getBackground().setColor(clickColor);
                labelTimeView.getStyle().setBackground(null);
                labelSettings.getStyle().setBackground(null);
            }
        });

        labelSettings.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                windowcell.setVisible(false);
                includefirst.setVisible(false);
                time.setVisible(false);
                settings.setVisible(true);

                labelSprayer.getStyle().setBackground(null);
                labelSprayerInfo.getStyle().setBackground(null);
                labelTimeView.getStyle().setBackground(null);
                labelSettings.getStyle().getBackground().setColor(clickColor);
            }
        });

        labelTimeView.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                windowcell.setVisible(false);
                includefirst.setVisible(false);
                settings.setVisible(false);
                panelTop.setVisible(false);
                time.setVisible(true);

                labelSprayer.getStyle().setBackground(null);
                labelSprayerInfo.getStyle().setBackground(null);
                labelTimeView.getStyle().getBackground().setColor(clickColor);
                labelSettings.getStyle().setBackground(null);
            }
        });
    }

    public void updateTheme(LayoutTheme theme) {
        super.updateTheme(theme);
        clickColor = theme.getThemeColor("#BFBFBF");
    }
}
