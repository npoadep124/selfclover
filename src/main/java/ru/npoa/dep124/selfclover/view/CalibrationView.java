package ru.npoa.dep124.selfclover.view;

import org.liquidengine.legui.component.ComponentXML;

public class CalibrationView extends ComponentXML {
    public CalibrationView() {

        super((path) -> CalibrationView.class.getResourceAsStream(path));

    }
}
