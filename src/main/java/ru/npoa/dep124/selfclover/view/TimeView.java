package ru.npoa.dep124.selfclover.view;

import annotations.LayoutComponentBind;
import annotations.LayoutIncludeBind;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Label;

public class TimeView extends ComponentXML {

    public TimeView() {
        super((path) -> TimeView.class.getResourceAsStream(path));
    }

}