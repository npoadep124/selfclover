package ru.npoa.dep124.selfclover.view;

import org.liquidengine.legui.component.ComponentXML;

public class IncludeFirstView extends ComponentXML  {

    public IncludeFirstView() {
        super((path) -> IncludeFirstView.class.getResourceAsStream(path));
    }

}
