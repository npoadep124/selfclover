package ru.npoa.dep124.selfclover.view;

import org.liquidengine.legui.component.Component;
import ru.npoa.dep124.plugin_core.ControllerViewInterface;

public class MainViewController implements ControllerViewInterface {

    MainView mainView;

    @Override
    public void setView(Component view) {
        mainView = (MainView) view;
    }
}
