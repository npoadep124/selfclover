package ru.npoa.dep124.selfclover.view;

import annotations.LayoutComponentBind;
import annotations.LayoutIncludeBind;
import annotations.LayoutPathBind;
import org.joml.Vector4f;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import parsing.LayoutTheme;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static org.liquidengine.legui.event.MouseClickEvent.MouseClickAction.PRESS;

@LayoutPathBind("/layout/layout_monitor_settings.xml")
public class MonitorSettingsView extends ComponentXML {

    @LayoutIncludeBind(xmlId = "monitor", xmlClass = MonitorView.class)
    private MonitorView monitor;

    @LayoutIncludeBind(xmlId = "equipment", xmlClass = EquipmentView.class)
    private EquipmentView equipment;

    @LayoutIncludeBind(xmlId = "calibration", xmlClass = CalibrationView.class)
    private CalibrationView calibration;

    @LayoutComponentBind()
    private Panel labelMonitor;

    @LayoutComponentBind()
    private Panel labelEquipment;

    @LayoutComponentBind()
    private Panel labelCalibrations;

    @LayoutComponentBind()
    private Panel labelEngine;

    @LayoutComponentBind()
    private Panel labelUpdate;

    @LayoutComponentBind()
    private Panel labelService;

    @LayoutComponentBind()
    private Panel labelAgrotronic;

    @LayoutComponentBind()
    private Label labelDateandTime;

    @LayoutComponentBind()
    private Label labelDateCalibration;

    @LayoutComponentBind()
    private Label labelTimeCalibration;

    private Timer timer = new Timer();
    private TimerTask taskTimer;
    private boolean taskRun = false;

    private Vector4f clickColorSettings = new Vector4f(0.22f, 0.66f, 0.33f, 1f);

    public MonitorSettingsView() {
    super((path) -> MonitorSettingsView.class.getResourceAsStream(path));

        DateFormat formatDateandTime = new SimpleDateFormat("dd.MM.yyyy/HH:mm");
        DateFormat formatDateCalibration = new SimpleDateFormat("dd.MM.yyyy");
        DateFormat formatTimeCalibration = new SimpleDateFormat("HH:mm:ss");

        monitor.initComp();
//        equipment.initComp();

        taskTimer = new TimerTask() {
            @Override
            public void run() {
                labelDateandTime.getTextState().setText(formatDateandTime.format(new Date()));
                labelDateCalibration.getTextState().setText(formatDateCalibration.format(new Date()));
                labelTimeCalibration.getTextState().setText(formatTimeCalibration.format(new Date()));
            }
        };
        timer = new Timer();
        taskRun = true;
        timer.scheduleAtFixedRate(taskTimer, 0, 1000);

        labelMonitor.getListenerMap().addListener(MouseClickEvent .class, (MouseClickEventListener) event -> {
        if (event.getAction().equals(PRESS)) {
            monitor.setVisible(true);
            equipment.setVisible(false);
            calibration.setVisible(false);


            labelMonitor.getStyle().getBackground().setColor(clickColorSettings);
            labelEquipment.getStyle().setBackground(null);
            labelCalibrations.getStyle().setBackground(null);
            labelEngine.getStyle().setBackground(null);
            labelUpdate.getStyle().setBackground(null);
            labelService.getStyle().setBackground(null);
            labelAgrotronic.getStyle().setBackground(null);
            }
        });

    labelEquipment.getListenerMap().addListener(MouseClickEvent .class, (MouseClickEventListener) event -> {
        if (event.getAction().equals(PRESS)) {
            monitor.setVisible(false);
            equipment.setVisible(true);
            calibration.setVisible(false);

            labelMonitor.getStyle().setBackground(null);
            labelEquipment.getStyle().getBackground().setColor(clickColorSettings);
            labelCalibrations.getStyle().setBackground(null);
            labelEngine.getStyle().setBackground(null);
            labelUpdate.getStyle().setBackground(null);
            labelService.getStyle().setBackground(null);
            labelAgrotronic.getStyle().setBackground(null);
        }
    });

    labelCalibrations.getListenerMap().addListener(MouseClickEvent .class, (MouseClickEventListener) event -> {
        if (event.getAction().equals(PRESS)) {
            monitor.setVisible(false);
            equipment.setVisible(false);
            calibration.setVisible(true);

            labelMonitor.getStyle().setBackground(null);
            labelEquipment.getStyle().setBackground(null);
            labelCalibrations.getStyle().getBackground().setColor(clickColorSettings);
            labelEngine.getStyle().setBackground(null);
            labelUpdate.getStyle().setBackground(null);
            labelService.getStyle().setBackground(null);
            labelAgrotronic.getStyle().setBackground(null);
        }
    });
}


    public void updateTheme(LayoutTheme theme) {
        super.updateTheme(theme);
        clickColorSettings = theme.getThemeColor("#F79F9F");
    }
}
