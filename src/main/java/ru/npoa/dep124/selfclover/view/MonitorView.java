package ru.npoa.dep124.selfclover.view;

import annotations.LayoutComponentBind;
import annotations.LayoutIncludeBind;
import annotations.LayoutPathBind;
import org.joml.Vector4f;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import parsing.LayoutTheme;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static org.liquidengine.legui.event.MouseClickEvent.MouseClickAction.PRESS;

//@LayoutPathBind("/layout/layout_monitor.xml")
public class MonitorView extends ComponentXML {

    @LayoutComponentBind()
    private Panel labelMonitorDataTime;

    @LayoutComponentBind()
    private Panel labelMonitorMain;

    @LayoutComponentBind()
    private Panel labelDT;

    @LayoutComponentBind()
    private Panel labelMonitorBack;

    @LayoutComponentBind()
    private Panel labelMonthUp;

    @LayoutComponentBind()
    private Panel labelMonthDown;

    @LayoutComponentBind()
    private Panel labelHourUp;

    @LayoutComponentBind()
    private Panel labelHourDown;

    @LayoutComponentBind()
    private Label labelTopTop;

    @LayoutComponentBind()
    private Label labelTop;

    @LayoutComponentBind()
    private Label labelCentral;

    @LayoutComponentBind()
    private Label labelBottom;

    @LayoutComponentBind()
    private Label labelBottomBottom;

    @LayoutComponentBind()
    private Label labelHourTopTop;

    @LayoutComponentBind()
    private Label labelHourTop;

    @LayoutComponentBind()
    private Label labelHourCentral;

    @LayoutComponentBind()
    private Label labelHourBottom;

    @LayoutComponentBind()
    private Label labelHourBottomBottom;

    @LayoutComponentBind()
    private Label labelMinutesTopTop;

    @LayoutComponentBind()
    private Label labelMinutesTop;

    @LayoutComponentBind()
    private Label labelMinutesCentral;

    @LayoutComponentBind()
    private Label labelMinutesBottom;

    @LayoutComponentBind()
    private Label labelMinutesBottomBottom;

    @LayoutComponentBind()
    private Panel labelMinutesUp;

    @LayoutComponentBind()
    private Panel labelMinutesDown;

    @LayoutComponentBind()
    private Label labelYearsTopTop;

    @LayoutComponentBind()
    private Label labelYearsTop;

    @LayoutComponentBind()
    private Label labelYearsCentral;

    @LayoutComponentBind()
    private Label labelYearsBottom;

    @LayoutComponentBind()
    private Label labelYearsBottomBottom;

    @LayoutComponentBind()
    private Panel labelYearsUp;

    @LayoutComponentBind()
    private Panel labelYearsDown;

    public MonitorView() {

        super((path) -> MonitorView.class.getResourceAsStream(path));

    }

    private final ArrayList<String> months = new ArrayList<String>();

    private String bottomBottomLabel;
    private String bottomLabel;
    private String topLabel;
    private String topTopLabel;
    private int centralIndex = 9;
    private String centralLabel;

    private int hourCount = 17;
    private String bbHoursLabel;
    private String bHoursLabel;
    private String tHoursLabel;
    private String ttHoursLabel;
    private String cHoursLabel;

    private int minutesCount = 39;
    private String bbMinutesLabel;
    private String bMinutesLabel;
    private String tMinutesLabel;
    private String ttMinutesLabel;
    private String cMinutesLabel;

    private int yearsCount = 2021;
    private String bbYearsLabel;
    private String bYearsLabel;
    private String tYearsLabel;
    private String ttYearsLabel;
    private String cYearsLabel;

    public void initComp() {

        months.add(0, "Январь");
        months.add(1, "Февраль");
        months.add(2, "Март");
        months.add(3, "Апрель");
        months.add(4, "Май");
        months.add(5, "Июнь");
        months.add(6, "Июль");
        months.add(7, "Август");
        months.add(8, "Сентябрь");
        months.add(9, "Октябрь");
        months.add(10, "Ноябрь");
        months.add(11, "Декабрь");



        labelMonitorMain.setVisible(true);
        labelMonitorDataTime.setVisible(false);


        labelDT.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                labelMonitorMain.setVisible(false);
                labelMonitorDataTime.setVisible(true);
                this.resizeAll();
            }
        });


        labelMonitorBack.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                labelMonitorMain.setVisible(true);
                labelMonitorDataTime.setVisible(false);
                this.resizeAll();
            }
        });


        labelMonthUp.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateMonth(1);
                labelBottomBottom.getTextState().setText(bottomBottomLabel);
                labelBottom.getTextState().setText(bottomLabel);
                labelCentral.getTextState().setText(centralLabel);
                labelTop.getTextState().setText(topLabel);
                labelTopTop.getTextState().setText(topTopLabel);

            }
        });

        labelMonthDown.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateMonth(0);
                labelBottomBottom.getTextState().setText(bottomBottomLabel);
                labelBottom.getTextState().setText(bottomLabel);
                labelCentral.getTextState().setText(centralLabel);
                labelTop.getTextState().setText(topLabel);
                labelTopTop.getTextState().setText(topTopLabel);
            }
        });

        labelHourUp.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateHour(0);
                labelHourBottomBottom.getTextState().setText(bbHoursLabel);
                labelHourBottom.getTextState().setText(bHoursLabel);
                labelHourCentral.getTextState().setText(cHoursLabel);
                labelHourTop.getTextState().setText(tHoursLabel);
                labelHourTopTop.getTextState().setText(ttHoursLabel);
            }
        });

        labelHourDown.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateHour(1);
                labelHourBottomBottom.getTextState().setText(bbHoursLabel);
                labelHourBottom.getTextState().setText(bHoursLabel);
                labelHourCentral.getTextState().setText(cHoursLabel);
                labelHourTop.getTextState().setText(tHoursLabel);
                labelHourTopTop.getTextState().setText(ttHoursLabel);
            }
        });

        labelMinutesUp.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateMinutes(0);
                labelMinutesBottomBottom.getTextState().setText(bbMinutesLabel);
                labelMinutesBottom.getTextState().setText(bMinutesLabel);
                labelMinutesCentral.getTextState().setText(cMinutesLabel);
                labelMinutesTop.getTextState().setText(tMinutesLabel);
                labelMinutesTopTop.getTextState().setText(ttMinutesLabel);
            }
        });

        labelMinutesDown.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateMinutes(1);
                labelMinutesBottomBottom.getTextState().setText(bbMinutesLabel);
                labelMinutesBottom.getTextState().setText(bMinutesLabel);
                labelMinutesCentral.getTextState().setText(cMinutesLabel);
                labelMinutesTop.getTextState().setText(tMinutesLabel);
                labelMinutesTopTop.getTextState().setText(ttMinutesLabel);
            }
        });

        labelYearsUp.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateYears(0);
                labelYearsBottomBottom.getTextState().setText(bbYearsLabel);
                labelYearsBottom.getTextState().setText(bYearsLabel);
                labelYearsCentral.getTextState().setText(cYearsLabel);
                labelYearsTop.getTextState().setText(tYearsLabel);
                labelYearsTopTop.getTextState().setText(ttYearsLabel);
            }
        });

        labelYearsDown.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if (event.getAction().equals(PRESS)) {
                UpdateYears(1);
                labelYearsBottomBottom.getTextState().setText(bbYearsLabel);
                labelYearsBottom.getTextState().setText(bYearsLabel);
                labelYearsCentral.getTextState().setText(cYearsLabel);
                labelYearsTop.getTextState().setText(tYearsLabel);
                labelYearsTopTop.getTextState().setText(ttYearsLabel);
            }
        });
    }

    public void UpdateMonth(int upDown) {
        if (upDown == 0) {
            if (centralIndex == 9) {
                centralIndex++;
                topTopLabel = months.get(centralIndex - 2);
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = " ";
            }
            else
            if (centralIndex == 10) {
                centralIndex++;
                topTopLabel = months.get(centralIndex - 2);
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = " ";
                bottomBottomLabel = " ";
            }
            else
            if(centralIndex < 9 && centralIndex > 0) {
                centralIndex++;
                topTopLabel = months.get(centralIndex - 2);
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = months.get(centralIndex + 2);

            }
            else
            if (centralIndex == 0) {
                centralIndex++;
                topTopLabel = " ";
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = months.get(centralIndex + 2);
            }
        }
        else {
            if (centralIndex == 2) {
                centralIndex--;
                topTopLabel = " ";
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = months.get(centralIndex + 2);
            }
            else
            if (centralIndex == 1) {
                centralIndex--;
                topTopLabel = " ";
                topLabel = " ";
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = months.get(centralIndex + 2);
            }
            else
            if(centralIndex > 2 && centralIndex < 11) {
                centralIndex--;
                topTopLabel = months.get(centralIndex - 2);
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = months.get(centralIndex + 2);
            }
            else
            if (centralIndex == 11) {
                centralIndex--;
                topTopLabel = months.get(centralIndex - 2);
                topLabel = months.get(centralIndex - 1);
                centralLabel = months.get(centralIndex);
                bottomLabel = months.get(centralIndex + 1);
                bottomBottomLabel = " ";
            }
        }
    }

    public void UpdateHour(int upDown) {
        if (upDown == 0) {
            if (hourCount == 2) {
                hourCount--;
                ttHoursLabel = " ";
                tHoursLabel = " ";
                cHoursLabel = Integer.toString(hourCount);
                bHoursLabel = Integer.toString(hourCount + 1);
                bbHoursLabel = Integer.toString(hourCount + 2);
            }
            else
                if (hourCount == 3) {
                    hourCount--;
                    ttHoursLabel = " ";
                    tHoursLabel = Integer.toString(hourCount - 1);
                    cHoursLabel = Integer.toString(hourCount);
                    bHoursLabel = Integer.toString(hourCount + 1);
                    bbHoursLabel = Integer.toString(hourCount + 2);
                }
                else
                    if (hourCount == 24) {
                        hourCount--;
                        ttHoursLabel = Integer.toString(hourCount - 2);
                        tHoursLabel = Integer.toString(hourCount - 1);
                        cHoursLabel = Integer.toString(hourCount);
                        bHoursLabel = Integer.toString(hourCount + 1);
                        bbHoursLabel = " ";
                    }
                    else
                        if(hourCount > 3 && hourCount < 24){
                            hourCount--;
                            ttHoursLabel = Integer.toString(hourCount - 2);
                            tHoursLabel = Integer.toString(hourCount - 1);
                            cHoursLabel = Integer.toString(hourCount);
                            bHoursLabel = Integer.toString(hourCount + 1);
                            bbHoursLabel = Integer.toString(hourCount + 2);
                        }
        }
        else {
            if (hourCount == 23) {
                hourCount++;
                ttHoursLabel = Integer.toString(hourCount - 2);
                tHoursLabel = Integer.toString(hourCount - 1);;
                cHoursLabel = Integer.toString(hourCount);
                bHoursLabel = " ";
                bbHoursLabel = " ";
            }
            else
                if (hourCount == 22) {
                    hourCount++;
                    ttHoursLabel = Integer.toString(hourCount - 2);
                    tHoursLabel = Integer.toString(hourCount - 1);
                    cHoursLabel = Integer.toString(hourCount);
                    bHoursLabel = Integer.toString(hourCount + 1);
                    bbHoursLabel = " ";
                }
                else
                    if (hourCount == 1) {
                        hourCount++;
                        ttHoursLabel = " ";
                        tHoursLabel = Integer.toString(hourCount - 1);
                        cHoursLabel = Integer.toString(hourCount);
                        bHoursLabel = Integer.toString(hourCount + 1);
                        bbHoursLabel = Integer.toString(hourCount + 2);
                    }
                    else
                        if(hourCount > 1 && hourCount < 22){
                            hourCount++;
                            ttHoursLabel = Integer.toString(hourCount - 2);
                            tHoursLabel = Integer.toString(hourCount - 1);
                            cHoursLabel = Integer.toString(hourCount);
                            bHoursLabel = Integer.toString(hourCount + 1);
                            bbHoursLabel = Integer.toString(hourCount + 2);
                        }
        }
    }

    public void UpdateMinutes(int upDown) {
        if (upDown == 0) {
            if (minutesCount == 2) {
                minutesCount--;
                ttMinutesLabel = " ";
                tMinutesLabel = " ";
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = Integer.toString(minutesCount + 2);
            }
            else
            if (minutesCount == 3) {
                minutesCount--;
                ttMinutesLabel = " ";
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = Integer.toString(minutesCount + 2);
            }
            else
            if (minutesCount == 60) {
                minutesCount--;
                ttMinutesLabel = Integer.toString(minutesCount - 2);
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = " ";
            }
            else
            if(minutesCount > 3 && minutesCount < 60){
                minutesCount--;
                ttMinutesLabel = Integer.toString(minutesCount - 2);
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = Integer.toString(minutesCount + 2);
            }
        }
        else {
            if (minutesCount == 59) {
                minutesCount++;
                ttMinutesLabel = Integer.toString(minutesCount - 2);
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = " ";
                bbMinutesLabel = " ";
            }
            else
            if (minutesCount == 58) {
                minutesCount++;
                ttMinutesLabel = Integer.toString(minutesCount - 2);
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = " ";
            }
            else
            if (minutesCount == 1) {
                minutesCount++;
                ttMinutesLabel = " ";
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = Integer.toString(minutesCount + 2);
            }
            else
            if(minutesCount > 1 && minutesCount < 58) {
                minutesCount++;
                ttMinutesLabel = Integer.toString(minutesCount - 2);
                tMinutesLabel = Integer.toString(minutesCount - 1);
                cMinutesLabel = Integer.toString(minutesCount);
                bMinutesLabel = Integer.toString(minutesCount + 1);
                bbMinutesLabel = Integer.toString(minutesCount + 2);
            }
        }
    }

    public void UpdateYears(int upDown) {
        if (upDown == 0) {
            if (yearsCount == 2019) {
                yearsCount--;
                ttYearsLabel = " ";
                tYearsLabel = " ";
                cYearsLabel = Integer.toString(yearsCount);
                bYearsLabel = Integer.toString(yearsCount + 1);
                bbYearsLabel = Integer.toString(yearsCount + 2);
            }
            else
            if (yearsCount == 2020) {
                yearsCount--;
                ttYearsLabel = " ";
                tYearsLabel = Integer.toString(yearsCount - 1);
                cYearsLabel = Integer.toString(yearsCount);
                bYearsLabel = Integer.toString(yearsCount + 1);
                bbYearsLabel = Integer.toString(yearsCount + 2);
            }
            else
            if(yearsCount > 2020){
                yearsCount--;
                ttYearsLabel = Integer.toString(yearsCount - 2);
                tYearsLabel = Integer.toString(yearsCount - 1);
                cYearsLabel = Integer.toString(yearsCount);
                bYearsLabel = Integer.toString(yearsCount + 1);
                bbYearsLabel = Integer.toString(yearsCount + 2);
            }
        }

        else {
            if (yearsCount == 2018) {
                yearsCount++;
                ttYearsLabel = " ";
                tYearsLabel = Integer.toString(yearsCount - 1);
                cYearsLabel = Integer.toString(yearsCount);
                bYearsLabel = Integer.toString(yearsCount + 1);
                bbYearsLabel = Integer.toString(yearsCount + 2);
            }
            else
            if(yearsCount > 2018) {
                yearsCount++;
                ttYearsLabel = Integer.toString(yearsCount - 2);
                tYearsLabel = Integer.toString(yearsCount - 1);
                cYearsLabel = Integer.toString(yearsCount);
                bYearsLabel = Integer.toString(yearsCount + 1);
                bbYearsLabel = Integer.toString(yearsCount + 2);
            }
        }
    }
}