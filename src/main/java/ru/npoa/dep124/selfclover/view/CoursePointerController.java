package ru.npoa.dep124.selfclover.view;


import org.liquidengine.legui.component.Component;
import ru.npoa.dep124.plugin_core.BaseViewControllerInterface;
import ru.npoa.dep124.plugin_core.ControllerViewInterface;
import ru.npoa.dep124.plugin_core.VehicleControlInterface;
import ru.npoa.dep124.plugin_core.VehicleState;
import ru.npoa.dep124.plugin_core.enums.AHRSStateIndicator;
import ru.npoa.dep124.plugin_core.enums.FluidState;
import ru.npoa.dep124.plugin_core.enums.HdopIndicator;
import ru.npoa.dep124.plugin_core.enums.SatelliteLinkIndicator;

import java.util.ArrayList;

public class CoursePointerController implements ControllerViewInterface {

    CoursePointerView coursePointerView;

    @Override
    public void setView(Component view) {
        coursePointerView = (CoursePointerView) view;
    }

}
