package ru.npoa.dep124.selfclover.view;

import annotations.LayoutComponentBind;
import annotations.LayoutIncludeBind;
import annotations.LayoutPathBind;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Label;
import ru.npoa.dep124.plugin_core.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


@LayoutPathBind("/layout/layout_main_frame.xml")
public class CoursePointerView extends ComponentXML {

    @LayoutComponentBind()
    private Label labelTime;
    @LayoutComponentBind()
    private Label labelDate;
    private Timer timer = new Timer();
    private TimerTask taskTimer;
    private boolean taskRun = false;


    public CoursePointerView() {
        super((path) -> CoursePointerView.class.getResourceAsStream(path));


        DateFormat formatOnlyTime = new SimpleDateFormat("HH:mm");
        DateFormat formatDate = new SimpleDateFormat("dd LLL E");

//        Label jj = new Label();
//        jj.getTextState().setText().setText();

            taskTimer = new TimerTask() {
                @Override
                public void run() {
//                    System.out.println("");
                    labelTime.getTextState().setText(formatOnlyTime.format(new Date()));
                    labelDate.getTextState().setText(formatDate.format(new Date()));
                }
            };
            timer = new Timer();
            taskRun = true;
            timer.scheduleAtFixedRate(taskTimer, 0, 1000); // запуск таймера

    }
}
