package ru.npoa.dep124.selfclover.view;

import org.liquidengine.legui.component.ComponentXML;

public class EquipmentView extends ComponentXML {

    public EquipmentView() {

        super((path) -> EquipmentView.class.getResourceAsStream(path));

    }
}
