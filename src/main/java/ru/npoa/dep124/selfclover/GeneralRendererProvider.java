package ru.npoa.dep124.selfclover;

import org.liquidengine.legui.AbstractInitializer;
import org.liquidengine.legui.CurrentInitializer;
import org.liquidengine.legui.component.Frame;
import org.liquidengine.legui.listener.processor.EventProcessorProvider;
import org.liquidengine.legui.system.context.CallbackKeeper;
import org.liquidengine.legui.system.context.Context;
import org.liquidengine.legui.system.context.DefaultCallbackKeeper;
import org.liquidengine.legui.system.handler.processor.SystemEventProcessor;
import org.liquidengine.legui.system.handler.processor.SystemEventProcessorImpl;
import org.liquidengine.legui.system.renderer.AbstractRendererProvider;
import org.liquidengine.legui.system.renderer.nvg.NvgRenderer;

public class GeneralRendererProvider extends AbstractInitializer {

    public <C extends AbstractRendererProvider> GeneralRendererProvider(long window, Frame frame, C render) {

        super.setFrame(frame);
        super.setWindow(window);

        // We need to create legui context which shared by renderer and event processor.
        // Also we need to pass event processor for ui events such as click on component, key typing and etc.
        super.setContext(new Context(window));

        // We need to create callback keeper which will hold all of callbacks.
        // These callbacks will be used in initialization of system event processor
        // (will be added callbacks which will push system events to event queue and after that processed by SystemEventProcessor)
        super.setCallbackKeeper(new DefaultCallbackKeeper());
        // register callbacks for window. Note: all previously binded callbacks will be unbinded.
        CallbackKeeper.registerCallbacks(window, super.getCallbackKeeper());

        // Event processor for system events. System events should be processed and translated to gui events.
        super.setSystemEventProcessor(new SystemEventProcessorImpl());
        SystemEventProcessor.addDefaultCallbacks(super.getCallbackKeeper(), super.getSystemEventProcessor());

        super.setGuiEventProcessor(EventProcessorProvider.getInstance());

        // Also we need to create initialize renderer provider
        // and create renderer which will render our ui components.
        super.setRenderer(new NvgRenderer(render));

        CurrentInitializer.setCurrentInitializer(this);
    }
}
