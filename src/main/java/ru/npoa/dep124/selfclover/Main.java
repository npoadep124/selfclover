package ru.npoa.dep124.selfclover;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


public class Main {
    private final static boolean run3dInSeparateThread = true;

    public final static int SYSTEM_EXIT = 0;
    public final static int SYSTEM_RESTART = 1;

    static ScheduledExecutorService uiExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r, "UI Thread");
            thread.setPriority(Thread.MIN_PRIORITY);

            return thread;
        }
    });

    static private void updateModeFromEnvAndProperties() {
    }

    /* main */
    public static void main(String[] args) {

        ViewController viewController = new ViewController(new BasicPlugin());

        if (run3dInSeparateThread)
            uiExecutor.scheduleAtFixedRate(viewController, 100, (int)(1000.0 / 25.0f), TimeUnit.MILLISECONDS);


    }

    public static void restartApplication(){
        System.exit(SYSTEM_RESTART);
    }

    public static void closeApplication(){
        System.exit(SYSTEM_EXIT);
    }

}

