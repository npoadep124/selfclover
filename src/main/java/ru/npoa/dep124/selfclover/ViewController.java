package ru.npoa.dep124.selfclover;

import org.joml.Vector2i;
import org.liquidengine.legui.AbstractInitializer;
import org.liquidengine.legui.animation.Animator;
import org.liquidengine.legui.animation.AnimatorProvider;
import org.liquidengine.legui.component.Component;
import org.liquidengine.legui.component.ComponentXML;
import org.liquidengine.legui.component.Frame;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.event.MouseDragEvent;
import org.liquidengine.legui.event.WindowSizeEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import org.liquidengine.legui.listener.MouseDragEventListener;
import org.liquidengine.legui.listener.WindowSizeEventListener;
import org.liquidengine.legui.style.color.ColorConstants;
import org.liquidengine.legui.style.font.FontRegistry;
import org.liquidengine.legui.system.context.Context;
import org.liquidengine.legui.system.layout.LayoutManager;
import org.liquidengine.legui.system.renderer.Renderer;
import org.liquidengine.legui.theme.Themes;
import org.liquidengine.legui.theme.colored.FlatColoredTheme;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengles.GLES;
import parsing.LayoutTheme;
import ru.npoa.dep124.plugin_core.AbstractMainView;
import ru.npoa.dep124.plugin_core.MainFrameViewInterface;
import ru.npoa.dep124.plugin_core.map.FollowMode;
import ru.npoa.dep124.plugin_core.map.ImageMarkers;
import ru.npoa.dep124.plugin_core.map.MapLayer;
import ru.npoa.dep124.plugin_core.map.ModeAB;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import static org.liquidengine.legui.event.MouseClickEvent.MouseClickAction.RELEASE;
import static org.liquidengine.legui.style.color.ColorUtil.fromInt;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengles.GLES20.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class ViewController implements Runnable  {
    private long window;
    private Frame frame;

    private AbstractInitializer renderInitializer;
    private Context context = null;
    private Renderer renderer;
    private LayoutTheme themeInitializer;

    private ComponentXML mainFrame;

    private int width;
    private int height;

    private BasicPlugin viewInterface;
    private Animator animator;

    // GLFW Callbacks
    private boolean resized;
    private boolean paused = false;
    private boolean locked = false;
    public boolean needsInit = false;
    public boolean needsClean = false;

    public static final Semaphore semaphore3d = new Semaphore(1,true);

    private float acc = 0;
    private float deltaTime;

    public ViewController(/*int displayMode,*/ BasicPlugin viewInterface)  {

        this.viewInterface = viewInterface;

        themeInitializer = this.viewInterface.initializationLayoutTheme();

        width = 1024;
        height = 770;

        Themes.setDefaultTheme(new FlatColoredTheme(
                fromInt(0, 0, 0, 1), // backgroundColor
                fromInt(176, 190, 197, 1), // borderColor
                fromInt(176, 190, 197, 1), // sliderColor
                fromInt(100, 181, 246, 1), // strokeColor
                fromInt(165, 214, 167, 1), // allowColor
                fromInt(239, 154, 154, 1), // denyColor
                ColorConstants.transparent(), // shadowColor
                ColorConstants.darkGray(), // text color
                FontRegistry.getDefaultFont(), // font
                16f //font size
        ));

    }
    class ScreenSize {
        public static final int width = 1024;
        public static final int height = 770;
//    public static final int height = 700;
    }

    private void createGuiElements(Frame frame) {
        mainFrame = (ComponentXML) viewInterface.getMainView();
        mainFrame.setFocusable(false);
        mainFrame.getListenerMap().addListener(WindowSizeEvent.class, (WindowSizeEventListener) event -> mainFrame.setSize(event.getWidth(), event.getHeight()));

        frame.getContainer().add(mainFrame);


        updateStyle();

//        mainFrame.hideAll();
//        mainFrame.showFirst();
    }

    private void updateStyle() {
        List<Component> child = frame.getContainer().getChildComponents();
        for (Component component : child)
            component.updateStyleLayout(themeInitializer);
    }

    public void initContext() {

        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        GLFWErrorCallback.createPrint().set();
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize glfw");

        glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_EGL_CONTEXT_API);
        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

//        window = glfwCreateWindow(width, height, viewInterface.getTitleWindows(), glfwGetPrimaryMonitor(), NULL);
        window = glfwCreateWindow(width, height, viewInterface.getTitleWindows(), NULL, NULL);
        if (window == NULL)
            throw new RuntimeException("Failed to create the GLFW window");
        glfwSetWindowPos(window, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2);

        glfwMakeContextCurrent(window);
        GLES.createCapabilities();
        glfwSwapInterval(1);

        glfwShowWindow(window);


        frame = new Frame(width, height);
        renderInitializer = new GeneralRendererProvider(window, frame, viewInterface.getRendererProvider());
        createGuiElements(frame);

        renderer = renderInitializer.getRenderer();
        renderer.initialize();

        context = renderInitializer.getContext();
        context.updateGlfwWindow();
        animator = AnimatorProvider.getAnimator();

        try
        {
            semaphore3d.acquire();
            //System.err.printf("Window init took: %f\n", Time.deltaTime());
            //System.err.printf("Logic init took: %f\n", Time.deltaTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally
        {
            semaphore3d.release();
        }
    }


    @Override
    public void run() {
        try {
            if (context == null)
                initContext();

            if (glfwWindowShouldClose(window)) {
                if (renderer != null)
                    renderer.destroy();
                glfwDestroyWindow(window);
                //            keyCallback.release();
                glfwTerminate();
                //            errorCallback.release();
                Main.closeApplication();
            }

            try {
                semaphore3d.acquire();

                if (needsClean) {
                    //System.err.printf("window needs clean start: %f\n", Time.getTime());
                    //System.err.println("Window closing");
                    cleanup();
                    needsClean = false;
                    //System.err.printf("window needs clean end: %f\n", Time.getTime());
                }

                //            if (window.isPaused())
                //            {
                //                try
                //                {
                //                    Thread.sleep(OPT_THREAD_SLEEP_TIME_MS);
                //                    continue;
                //                }
                //                catch (Exception e){
                //                    e.printStackTrace();
                //                }
                //            }


                if (needsInit) {
                    //System.err.printf("window needs init start: %f\n", Time.getTime());
                    //System.err.println("Window opening -- add history");
                    reinit();
                    //logic.addHistory();
                    needsInit = false;
                    //System.err.printf("window needs init end: %f\n", Time.getTime());
                }


//                deltaTime = Time.deltaTime();
                acc += deltaTime;


            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                semaphore3d.release();
            }

            Vector2i windowSize = context.getFramebufferSize();
            //        System.out.println("win " + windowSize.x +" "+ windowSize.y);

            glClearColor(0, 0, 0, 0);
            // Set viewport size
            glViewport(0, 0, windowSize.x, windowSize.y);
            // Clear screen
            glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
            LayoutManager.getInstance().layout(frame);
            renderer.render(frame, context);
            glfwPollEvents();

            glfwSwapBuffers(window);

            animator.runAnimations();
            // Now we need to handle events. Firstly we need to handle system events.
            // And we need to know to which frame they should be passed.
            renderInitializer.getSystemEventProcessor().processEvents(frame, context);

            // When system events are translated to GUI events we need to handle them.
            // This event processor calls listeners added to ui components
            renderInitializer.getGuiEventProcessor().processEvents();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void cleanup() {
        //TODO
    }


    public void reinit(){

    }


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


    public LayoutTheme getThemeInitializer()
    {
        return themeInitializer;
    }

    //--------------------------
}
