#!/bin/bash

WDIR="$(pwd)"
WTMP="${WDIR}/$1"

GIT_SHORT_HASH=$(git log --oneline HEAD | head -n 1 | awk '{ print $1 }')
#GIT_DESCRIBE=$(git describe --dirty --always --long --tags)
GIT_DESCRIBE=$(git describe --dirty --always --long)
BUILD_DATETIME=$(LANG=C date +"%Y-%m-%d %H:%M:%S")
BUILD_DATE=$(LANG=C date +"%y%m%d")
NAME_PROJECT=$3

mkdir -p ${WTMP}

cat > ${WTMP}/src/main/java/ru/npoa/dep124/parallax/VersionApp.java << EOF

package ru.npoa.dep124.parallax;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class VersionApp {
    final static String VERSION_STRING = "$2 (${GIT_DESCRIBE})";
    final static String VERSION_BUILD_DATE = "${BUILD_DATETIME}";
    final static String VERSION_TO_MTU_STRING = "${GIT_SHORT_HASH:0:4}${BUILD_DATE}";

    public VersionApp(){
    }

    public static void createDefaultFiles(){
	String[] version_ini = new String[]{	
        "[${NAME_PROJECT}-$2]",
        "name=${NAME_PROJECT}-$2.jar",
        "version=${GIT_DESCRIBE} от ${BUILD_DATETIME}",
        "description=is default files",
        "size=0"};
        
        String currver = "${NAME_PROJECT}-$2.jar";

        String fileNameVer = getPathToDefaultFile() + "/versions.ini";
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(fileNameVer));
            for (String row : version_ini) {
                if (row.indexOf("size=") != -1) {
                    row = "size=" + String.valueOf(getSizeJar());
                }
                bw.write(row);
                bw.write("\n");
            }
            bw.close();
        } catch (IOException ex) {
        }

        String fileNameCurr = getPathToDefaultFile() + "/currver";
        try {
            bw = new BufferedWriter(new FileWriter(fileNameCurr));
            bw.write(currver);
            bw.close();
        } catch (IOException ex) {
        }
        
    }
    public static long getSizeJar(){
        long res = 0l;
        try {

            File fileJar = new File(VersionApp.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            res = fileJar.length();
        }
        catch (Exception ex){

        }
        return res;
    }
    public static String getPathToDefaultFile() {
        String currPath = "";
        try {
            String pathToJar = VersionApp.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            currPath = pathToJar.substring(0,pathToJar.lastIndexOf("/"));

        } catch (Exception ex) {

        }
        return currPath;
    }
}

EOF

exit 0

